#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define MAXSIZE 1000
float mean_val(float[],int);
float median_val(float[],int);
float mode_val(float[],int);
int main(char argc, char **argv)
{
	FILE *ptr_file;
	FILE *out_file;
	char buffer[1000];
	char inbuffer[1000];
	char *value;
	char *inputval[MAXSIZE];
	int i=0,found = 0,k=0,graph=0;
	int n,ch;
	float array[MAXSIZE],mean,median,mode;

	ptr_file =fopen(argv[1],"r");
	if (!ptr_file)
		return 1;

	out_file = fopen("output.c","a+");
	if (!out_file)
		return 1;

	while (fgets(buffer,1000, ptr_file)!=NULL)
	{
		value = strtok(buffer," ,:;\n\t");
		while (value != NULL)
		{
	    	inputval[i] = strdup(value);
	    	value = strtok (NULL, " ,:;\n\t");
	    	i++;
		}

	}

	n = i;
	for(k=0; k<n; k++)
	{
		array[k] = atof(inputval[k]);
	}

	if (strcmp(argv[2],"4") == 0)
	{	
		ch=4;
		graph=1;
	}
	do
	{
	if (strcmp(argv[2],"4") != 0)
	{	
		printf("\n\tEnter Choice:\n\t1.Mean\n\t2.Median\n\t3.Mode\n\t4.Write to File\n\t5.Exit\n\t");
		scanf("%d",&ch);
	}	
	switch(ch)
	{
	case 1:
	mean=mean_val(array,n);
	printf("\n\tMean : %f\n",mean);
	break;
	case 2:
	median=median_val(array,n);
	printf("\n\tMedian :%f\n",median);
	break;
	case 3:
	mode=mode_val(array,n);
	printf("\n\tMode :%f\n",mode);
	break;
	case 4:
	if(out_file){
		fclose(out_file);
		out_file = fopen("output.c","a+");
	}
	mean=mean_val(array,n);
	median=median_val(array,n);
	mode=mode_val(array,n);
	
	

	while (fgets(inbuffer,1000, out_file)!=NULL)
	{
		if((strstr(inbuffer, "Mean")) != NULL) {
		found=1;
		}
	}
	if(found==0)
	{

	fprintf(out_file,"Mean = %f\n",mean);
	fprintf(out_file,"Median = %f\n",median);
	fprintf(out_file,"Mode = %f\n",mode);
	}
	if (graph==0)
		break;
	else
		exit(0);
	case 5:
	break;
	default:
	printf("Invalid Option");
	break;
	}
	}while(ch!=5);

	fclose(ptr_file);
	fclose(out_file);
    return 0;
}

float mean_val(float array[],int n)
{
int i;
float s=0;
for(i=0;i<n;i++)
s=s+array[i];
return (s/n);
}

float median_val(float a[],int n)
{
float t;
int i,j;
for(i=0;i<n;i++)
for(j=i+1;j<n;j++)
{
if(a[i]>a[j])
{
t=a[j];
a[j]=a[i];
a[i]=t;
}
}
if(n%2==0)
return (a[n/2]+a[n/2-1])/2;
else
return a[n/2];
}


float mode_val(float a[],int n)
{

int  i, j,k, mod[1000]={0},cnt=0, t=0, f=0;
float big;
  
   for(i=0;i<n;i++){
        cnt=0;
        for(j=i+1;j<=n;j++){
            if(a[i]==a[j]){
                cnt++;
            }
        }
        if(t<cnt){
            t=cnt;
            mod[0]=a[i];
            mod[1]='\0';
        }
        else if(t == cnt){
            f=0;
            for(k=0;mod[k]!='\0';k++){
                if(a[i]==mod[k]){
                    f=1;
                }
            }
            if(f==0){
                for(k=0;mod[k]!='\0';k++);
                mod[k]=a[i];
                mod[++k]='\0';
            }
        }
    }

big=mod[0];
for(i=1;mod[i]!='\0';i++)
{

	if((mod[i]>big) && mod[i]!='\0')
	{
		big=mod[i];
	}	
}
return big; 
}





