#!/bin/sh
clear
gcc mean.c -o meanexe
gcc range.c -o rangeexe -lm
cat /dev/null >output.c
echo "Enter input filename : " 
read ipFilename

while true; do

echo '\n\t' "Choose any of the below option : " '\n\t' "A : Mean, Median, Mode " '\n\t' "B : Range, InterQuartile Range, Standard Deviation, CV " '\n\t' "C : Graphs  "'\n\t' "E : Exit "
read Option

case "$Option" in
  A)
   
    ./meanexe $ipFilename 0
    ;;
  B)

    ./rangeexe $ipFilename 0
    ;;
  C) 
   cat /dev/null > output.c
   ./meanexe $ipFilename 4
   ./rangeexe $ipFilename 5
   
     libreoffice --calc /home/mtech/Desktop/neeraS/asg/CTGRAPH.ods

    ;;
  E)
    echo "Good Bye "
    break
    ;;
  *)
    echo "Option is invalid, please try again "
    ;;
esac
done

neera sali
