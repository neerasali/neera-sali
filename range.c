#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <math.h>
#define MAXSIZE 1000
FILE *f;
int rangef(int[],int);
int iqrf(int[],int);
float sdf(int[],int);
float cvf(int[],int);
int main(char argc, char **argv)
{
	FILE *fp;
	char buffer[1000];
	char ibuff[1000];
	char *value;
	char *inputval[MAXSIZE];
	int i=0,j=0,a[MAXSIZE],n=0,k,tp,ch,range,iqr,fo=0,graph=0;
	float sd,cv;

	fp = fopen(argv[1],"r");
	f = fopen("output.c","a+");

	while (fgets(buffer,1000, fp)!=NULL)
	{
		value = strtok(buffer," ,:;\n\t");
		while (value != NULL)
		{
	    	inputval[i] = strdup(value);
	    	value = strtok (NULL, " ,:;\n\t");
	    	i++;
		}

	}

	n = i;
	for(k=0; k<n; k++)
	{
		a[k] = atof(inputval[k]);
	}

	//Sort the Input array
	for(i=0;i<n;i++)
	{
		for(j=i+1;j<n;j++)
		{
			if(a[i]>a[j])
			{
				tp=a[i];
				a[i]=a[j];
				a[j]=tp;
			}
		}
	}
	
	if (strcmp(argv[2],"5") == 0)
	{	
		ch=5;
		graph=1;
	}

	do {
	if (strcmp(argv[2],"5") != 0)
	{
	printf("\n\t1.Range \n\t2.Interquartile range \n\t3.Standard Deviation \n\t4.Coefficient of Variation\n\t5.Write to File\n\t6.Exit\n\tEnter your choice: ");
	scanf("%d",&ch);
	}
	switch(ch)
	{
		case 1 : range=rangef(a,n);
			 printf("\t\tRange = %d",range);
			 break;
		case 2 : iqr=iqrf(a,n);
			 printf("\t\tInterquartile range = %d",iqr);
			 break;
		case 3 : sd=sdf(a,n);
			 printf("\t\tStandard Deviation = %f",sd);
			 break;
		case 4 : cv=cvf(a,n);
			 printf("\t\tCoefficient of variation = %f",cv);
			 break;
		case 5 :
		 	 range=rangef(a,n);
		 	 iqr=iqrf(a,n);
		 	 sd=sdf(a,n);
		 	 cv=cvf(a,n);
		 	 if(f)
			 {
			   fclose(f);
			   f = fopen("output.c","a+");
			 }
			 while (fgets(ibuff,1000, f)!=NULL)
			 {
			  if((strstr(ibuff, "Range")) != NULL) {
			   fo=1;
 			  }
			 }
			 if(fo==0)
			 {
			 fprintf(f,"Range = %d \n",range);
		 	 fprintf(f,"IQR = %d \n",iqr);
		 	 fprintf(f,"SD = %f \n",sd);
		 	 fprintf(f,"CV = %f \n",cv);
			 }
		
			 if (graph==0)
				break;
			 else
				exit(0);
		case 6:
		 	 break;
		default :printf("\n Invalid Option");
	}

	}while(ch!=6);

  fclose(fp);
  fclose(f);
  return 0;

}

int rangef(int a[],int n){
	int range=a[n-1]-a[0];
	return range;
}

int iqrf(int a[], int n)
{
	int tot;
	if(n%2==0){
		tot = n;
	}
	else{
		tot = n+1;
	}
	int l = (.25)*tot;
	int u = (.75)*tot;
        float lower = (a[l-1]+a[l])/2;
	float upper = (a[u-1]+a[u])/2;
	float total = upper-lower;
	return total;
}

float sdf(int a[], int n){
	float avg=0,c=0,var=0;
	float sd =0,b[1000];
	int i;
	for(i=0;i<n;i++)
			avg=avg+a[i];
			 
			avg=avg/n;
			 for(i=0;i<n;i++)
			 {
				c=a[i]-avg;
				c=c*c;
				b[i]=c;
			 }
 
			for(i=0;i<n;i++)
				var=var+b[i];
			var=var/n;
	
			sd=sqrt(var);
		
			return sd;

}

float cvf(int a[], int n)
{
	int i;
	float avg=0;
	float sd=0,cv=0;
	for(i=0;i<n;i++)
		avg=avg+a[i];
	avg=avg/n;
	sd= sdf(a,n);
	cv=sd/avg;
	return cv;
}

neera sali
